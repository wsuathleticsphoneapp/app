var $ = require('jquery');
var sjcl = require('sjcl');
var baseUrl = "http://ec2-52-91-191-229.compute-1.amazonaws.com:3000/api";

module.exports = {
    get: function (url) {
        return new Promise(function (success, error) {
            $.ajax({
                headers: {"Authorization": localStorage.getItem("access_token")},
                url: baseUrl + url,
                dataType: "json",
                success: success,
                error: error
            })
        })
    },
    post: function (url, data) {
        return new Promise(function (success, error) {
            $.ajax({
                headers: {"Authorization": localStorage.getItem("access_token")},
                url: baseUrl + url,
                type: "POST",
                data: data,
                success: success,
                error: error
            })
        })
    },
    put: function (url, data) {
        return new Promise(function (success, error) {
            $.ajax({
                headers: {"Authorization": localStorage.getItem("access_token")},
                url: baseUrl + url,
                type: "PUT",
                data: data,
                success: success,
                error: error
            })
        })
    },
    delete: function (url) {
        return new Promise(function (success, error) {
            $.ajax({
                headers: {"Authorization": localStorage.getItem("access_token")},
                url: baseUrl + url,
                type: "DELETE",
                success: success,
                error: error
            })
        })
    },
    hash: function (string) {
        var stringBits = sjcl.hash.sha256.hash(string);
        return sjcl.codec.hex.fromBits(stringBits);
    }
};