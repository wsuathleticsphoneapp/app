var React = require('react');
var helper = require('./../rest-helper.js');
var ReactRouter = require('react-router');
var TeamList = require('../team-list.js');
var Link = ReactRouter.Link;

var CreateUser = React.createClass({
    getInitialState: function () {
        return {firstName: "", lastName: "", teamId: "", email: "", password: "", password2: "", teams: []};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    componentDidMount: function () {
        var mContext = this;
        helper.get("/Teams?", mContext)
            .then(function (data) {
                mContext.setState({teams: data});
            });
    },
    handleFirstName: function (e) {
        this.setState({firstName: e.target.value});
    },
    handleLastName: function (e) {
        this.setState({lastName: e.target.value});
    },
    handleTeam: function (e) {
        this.setState({teamId: e.target.value});
    },
    handleEmail: function (e) {
        this.setState({email: e.target.value});
    },
    handlePassword: function (e) {
        this.setState({password: e.target.value});
    },
    handlePassword2: function (e) {
        this.setState({password2: e.target.value});
    },
    attemptCreateUser: function (e) {
        e.preventDefault();

        if (this.state.password != this.state.password2) {
            alert("Passwords don't match");
            return;
        }
        if (this.state.password.length < 4) {
            alert("Make a better password");
            return;
        }
        if (this.state.firstName.length < 1) {
            alert("First name is required");
            return;
        }
        if (this.state.lastName.length < 1) {
            alert("Last name is required");
            return;
        }

        var newUser = {
            "username": this.state.email,
            //TODO should probably take out one of these emails
            "email": this.state.email,
            "password": helper.hash(this.state.password),
            "FirstName": this.state.firstName,
            "LastName": this.state.lastName
            //TODO put in team
        };

        console.log("creating user");
        var mContext = this;
        helper.post("/users", newUser, mContext)
            .then(function (value) {
                console.log("created");
                helper.post("/users/login", newUser)
                    .then(function (value) {
                        console.log("logged in");
                        localStorage.setItem("access_token", value.id);
                        localStorage.setItem("id", value.userId);
                        mContext.context.router.replace("/dashboard")
                    }, function (reason) {
                        console.log("couldn't log in: " + reason.message + reason.error);
                        alert("Account created, but log in failed"); // Error!
                    });
            }, function (reason) {
                //response 422 username exists already
                if (reason.status == "422") {
                    alert("An account with this email already exists"); // Error!
                }
                else {
                    alert("Couldn't create account");
                }
                console.log("couldn't create user: " + reason.message + reason.error);
            });

    },
    render: function () {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6">
                        <form className="form-horizontal" onSubmit={this.attemptCreateUser}>
                            <fieldset>
                                <div id="legend">
                                    <legend className="white">Sign Up</legend>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="teamId">Team</label>
                                    <div className="controls">
                                        <TeamList mContext={this}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="firstname">First Name</label>
                                    <div className="controls">
                                        <input type="text" id="firstname" name="firstname" placeholder="John"
                                               className="form-control input-lg" value={this.state.firstName}
                                               onChange={this.handleFirstName}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="lastname">Last Name</label>
                                    <div className="controls">
                                        <input type="text" id="lastname" name="lastname" placeholder="Smith"
                                               className="form-control input-lg" value={this.state.lastName}
                                               onChange={this.handleLastName}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="email">Email</label>
                                    <div className="controls">
                                        <input type="email" id="email" name="email" placeholder="asdf@qwerty.com"
                                               className="form-control input-lg" value={this.state.email}
                                               onChange={this.handleEmail}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="password">Password</label>
                                    <div className="controls">
                                        <input type="password" id="password" name="password" placeholder="*******"
                                               className="form-control input-lg" value={this.state.password}
                                               onChange={this.handlePassword}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="password">Confirm Password</label>
                                    <div className="controls">
                                        <input type="password" id="password" name="password" placeholder="*******"
                                               className="form-control input-lg" value={this.state.password2}
                                               onChange={this.handlePassword2}/>
                                    </div>
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="col-xs-6 col-sm-6 col-md-6">
                                        <button className="btn btn-lg btn-success btn-block">Register</button>
                                    </div>
                                    <div className="col-xs-6 col-sm-6 col-md-6">
                                        <Link to="/login" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = CreateUser;