var React = require('react');

var UserList = React.createClass({
    render: function() {
        var mContext = this.props.mContext;
        var data = mContext.state.users;
        var users = data.map(function(user) {
            return (
                <option value={user.id}>{user.username}</option>
            )
        });
        return (
            <select onChange={mContext.handleUser} className="drop-down">
                {users}
            </select>
        );
    }
});

module.exports = UserList;