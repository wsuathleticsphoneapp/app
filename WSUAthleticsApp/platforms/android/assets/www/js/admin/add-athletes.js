var React = require('react');
var helper = require('../rest-helper.js');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

var AddAthletes = React.createClass({
    getInitialState: function () {
        return {emails: ""};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    handleEmails: function (e) {
        this.setState({emails: e.target.value});
    },
    attemptWhiteList: function (e) {
        e.preventDefault();
        //var emailArray = this.state.emails.trim.split(","); //TODO check for @ and .?
        var emailArray = this.state.emails.replace(/ /g, '').split(",");
        console.log(emailArray[0] + " " + emailArray[1] + " " + emailArray[2]);
            //var newUser = {
            //    "username": helper.hash(this.state.athleteId),
            //    "email": this.state.firstName + "@" + this.state.lastName + ".com",
            //    "id": localStorage.getItem("id")
            //};
            //
            //var mContext = this;
            //helper.put("/users/" + newUser.id, newUser, mContext)
            //    .then(function (value) {
            //        console.log("Athlete updated: " + value);
            //        mContext.context.router.replace("/admin/")
            //    }, function (reason) {
            //        console.log("couldn't update user: " + reason.status + " " + reason.responseText); // Error!
            //    });
    },
    render: function () {
        return (
            <div className = "container">
                <form className="form-horizontal" onSubmit={this.attemptWhiteList}>
                    <div id="legend">
                        <legend className="white">Add emails to whitelist</legend>
                    </div>
                    <div className="control-group">
                        <label className="control-label" htmlFor="emails">Emails</label>
                        <div className="controls">
                            <textarea
                                   id="emails"
                                   rows="10"
                                   name="emails"
                                   placeholder="bruhseph@msn.com, broseph@hotmail.com, ..."
                                   className="form-control input-lg"
                                   value = {this.state.emails} onChange = {this.handleEmails} />
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" className="btn btn-lg btn-success btn-block" value="Submit"/>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <Link to="/admin/" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
});

module.exports = AddAthletes;