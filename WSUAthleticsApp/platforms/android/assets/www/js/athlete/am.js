var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;
//TODO get these to be right justified when displayed
var sleepOptions = ["Insomnia", "Restless sleep", "Difficulty falling asleep", "Good", "Very restful"];
var fatigueOptions = ["Always tired", "More tired than normal", "Normal", "Fresh", "Very fresh"];
var sorenessOptions = ["Very sore", "Increase in soreness/tightness", "Normal", "Feeling good", "Feeling great"];
var moodOptions = ["Highly annoyed/irritable/down", "Aggravated/short tempered", "Less interested in others or activities than usual",
                        "Generally good mood", "Very positive mood"];
var stressOptions = ["Highly stressed", "Feeling stressed", "Normal", "Relaxed", "Very relaxed"];


var Am = React.createClass({
    getInitialState: function () {

        //TODO make a dropdown menu of today's date, and yesterday's date
        //depending on the date, retrieve the survey from that date and autofill if it isn't null
        //do the same thing in post practice surveys or something
        //remove the past surveys page
        return {sleep: 2, fatigue: 2, stress: 2, mood: 2, soreness: 2, completed: ["not complete","not complete"]};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    handleHeartRate: function (e) {
        this.setState({heartRate: e.target.value});
    },
    handleSleep: function (e) {
        this.setState({sleep: e.target.value});
    },
    handleFatigue: function (e) {
        this.setState({fatigue: e.target.value});
    },
    handleStress: function (e) {
        this.setState({stress: e.target.value});
    },
    handleMood: function (e) {
        this.setState({mood: e.target.value});
    },
    handleSoreness: function (e) {
        this.setState({soreness: e.target.value});
    },
    submitSurvey: function (e) {
        e.preventDefault();
        console.log("Submitting survey: ", this.state.heartRate + " " + this.state.sleep);
        this.context.router.push("/dashboard")
    },
    render: function () {
        return (
            <div className = "container">
                        <form className="form-signin" onSubmit={this.submitSurvey}>
                            <h3>AM Survey</h3>

                            <div>
                                <label htmlFor="day">Day</label>
                                <br/>
                                <select className="drop-down">
                                    <option value={0}>Today ({this.state.completed[0]})</option>
                                    <option value={1}>Yesterday ({this.state.completed[1]})</option>
                                </select>
                            </div>

                            <label htmlFor="heartRate">Resting Heart Rate</label>
                            <input value={this.state.heartRate} onChange={this.handleHeartRate} type="number" id="heartRate"
                                   className="form-control" placeholder="Heart Rate" min="20" max="120" required="" autofocus=""/>
                            <br/>
                            <label htmlFor="sleep">Quality of Sleep: {sleepOptions[this.state.sleep]}</label>
                            <input value={this.state.sleep} onChange={this.handleSleep} type="range" min="0" max="4" id="sleep" />
                            <br/>
                            <label htmlFor="fatigue">Fatigue: {fatigueOptions[this.state.fatigue]}</label>
                            <input value={this.state.fatigue} onChange={this.handleFatigue} type="range" min="0" max="4" id="fatigue" />
                            <br/>
                            <label htmlFor="stress">Stress: {stressOptions[this.state.stress]}</label>
                            <input value={this.state.stress} onChange={this.handleStress} type="range" min="0" max="4" id="stress" />
                            <br/>
                            <label htmlFor="mood">Mood: {moodOptions[this.state.mood]}</label>
                            <input value={this.state.mood} onChange={this.handleMood} type="range" min="0" max="4" id="mood" />
                            <br/>
                            <label htmlFor="soreness">Soreness: {sorenessOptions[this.state.soreness]}</label>
                            <input value={this.state.soreness} onChange={this.handleSoreness} type="range" min="0" max="4" id="soreness" />
                            <br/>
                            <div className="row">
                                <div className="col-xs-6 col-sm-6 col-md-6">
                                    <input type="submit" className="btn btn-lg btn-success btn-block" value="Submit"/>
                                </div>
                                <div className="col-xs-6 col-sm-6 col-md-6">
                                    <Link to="/dashboard" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                                </div>
                            </div>
                        </form>
            </div>
        )
    }
});

module.exports = Am;