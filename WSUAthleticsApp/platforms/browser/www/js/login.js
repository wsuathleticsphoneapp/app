var React = require('react');
var helper = require('./rest-helper.js');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

var Login = React.createClass({
    getInitialState: function () {
        return {email: "", password: ""};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    handleAthleteId: function (e) {
        this.setState({email: e.target.value});
    },
    handlePassword: function (e) {
        this.setState({password: e.target.value});
    },
    attemptLogin: function (e) {
        e.preventDefault();
        var user = {
            "username": this.state.email,
            "password": helper.hash(this.state.password)
        };
        var mContext = this;

        helper.post("/users/login", user, mContext)
            .then(
                function (value) {
                    console.log("Success!: " + value.id); // Success! Logged in!
                    localStorage.setItem("access_token", value.id);
                    localStorage.setItem("id", value.userId);
                    mContext.context.router.replace("/dashboard");
                }, function (reason) {
                    if(reason.status == "401")
                    {
                        alert("Invalid credentials ;_;");
                    }
                    else
                        alert("Couldn't Log in ;_;");
                }
            );
    },
    render: function () {
        return (
            <div className="container">
                <form className="form-signin" onSubmit={this.attemptLogin}>
                    <h2 className="form-signin-heading">Please sign in</h2>
                    <label htmlFor="email" className="sr-only">Email</label>
                    <input value={this.state.email} onChange={this.handleAthleteId} type="email" id="email"
                           className="form-control" placeholder="asdf@qwerty.com" required="" autofocus=""/>
                    <br/>
                    <label htmlFor="inputPassword" className="sr-only">Password</label>
                    <input type="password" value={this.state.password} id="inputPassword" className="form-control"
                           placeholder="Password" onChange={this.handlePassword} required=""/>
                    <br/>
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" className="btn btn-lg btn-success btn-block" value="Sign In"/>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <Link to="/create-account" className="btn btn-lg btn-primary btn-block">Sign Up</Link>
                        </div>
                    </div>
                </form>
                <Link to="/forgot-password">Forgot Password?</Link>
            </div>
        )
    }
});

module.exports = Login;
