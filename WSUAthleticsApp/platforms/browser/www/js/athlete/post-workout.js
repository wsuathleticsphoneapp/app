var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

var PostWorkout = React.createClass({
    getInitialState: function () {
        return {intensity: 5, completed: ["not complete","not complete"], day:""};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    handleWorkoutTime: function (e) {
        this.setState({workoutTime: e.target.value});
    },
    handleIntensity: function (e) {
        this.setState({intensity: e.target.value});
    },
    handleDay: function(e) {
        this.setState({day: e.target.value});
        console.log(this.state.day);
    },
    submitSurvey: function (e) {
        e.preventDefault();
        console.log("Submitting survey: ", this.state.workoutTime + " " + this.state.intensity);
        this.context.router.push("/dashboard")
    },
    render: function () {
        return (
            <div className="container">
                <form className="form-signin" onSubmit={this.submitSurvey}>

                    <div>
                        <label htmlFor="day">Day</label>
                        <br/>
                        <select className="drop-down" onChange={this.handleDay}>
                            <option value={0}>Today ({this.state.completed[0]})</option>
                            <option value={1}>Yesterday ({this.state.completed[1]})</option>
                        </select>
                    </div>

                    <label htmlFor="workoutTime" className="sr-only">Workout Time (minutes)</label>
                    <input id="workoutTime" className="form-control" value={this.state.workoutTime}
                           onChange={this.handleWorkoutTime} placeholder="Workout Time (minutes)"/>
                    <br/>
                    <label htmlFor="intensity">Practice/Training Session Intensity</label>
                    <input id="intensity" type="range" min="0" max="10" value={this.state.intensity} onChange={this.handleIntensity}/>
                    {this.state.intensity}
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" className="btn btn-lg btn-success btn-block" value="Submit"/>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <Link to="/dashboard" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                        </div>
                    </div>
                </form>

            </div>

        )
    }
});

module.exports = PostWorkout;