var React = require('react');
var helper = require('./../rest-helper.js');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

//TODO either pass date to survey, or generate it all here
//or just remove it
var SurveyList = React.createClass({
    render: function() {
        var surveys = this.props.data.map(function(survey) {
            return (
                <div>
                    <Link to="/am">{survey.SurveyName}</Link>
                </div>
            )
        });
        return (
            <div>
                {surveys}
            </div>
        );
    }
});


var PastSurveys = React.createClass({
    getInitialState: function () {
        return {data: []};
    },
    componentDidMount: function() {
        var surveys = this;
        helper.get("http://wsudeploy-brysonwilding.rhcloud.com:80/api/Surveys", surveys)
            .then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    surveys.setState({data: data});
                }
            });
    },
    render: function () {
        return (
            <div>
                <h3>Am Surveys</h3>
                <SurveyList data={this.state.data}/>
                <h3>Post Workout/Training Session Surveys</h3>

            </div>
        )
    }
});

module.exports = PastSurveys;