var React = require('react');
var helper = require('../rest-helper.js');
var ReactRouter = require('react-router');
var TeamList = require('../team-list.js');
var UserList = require('../user-list.js');
var SurveyList = require('../report-list.js');
var Link = ReactRouter.Link;
var DatePicker = require('react-datepicker');
var moment = require('moment');
//var LineChart = require('react-chartjs').Line;
var Chart = require('react-google-charts').Chart;
var StudentData = require('../StudentData');
var CodeHolder = require('../CodeHolder');
var ss = require('simple-statistics');

var Reports = React.createClass({
    getInitialState: function () {

        //TODO this is how you can add a linear regression line
        var reg = ss.linearRegression([[0, 0], [1, 1], [2,3]]);
        //y = mx + b
        console.log("m: " + reg.m + " b: " + reg.b);

        //TODO add in a row to the team members that says something like, "show all"
        //this option will make the scatter plot of all the athletes be shown instead
        // of the line graph. Something with linear regression would be pretty cool...
        return {
            reg:  reg,
            startDate: moment(),
            teams:[],
            surveys:[],
            users:[],
            StudentDataChart: {
                rows:[],
                columns:[],
                chartType: ""
            }
        };
    },
    /*contextTypes: {
     router: React.PropTypes.object.isRequired
     },*/
    componentDidMount: function() {
        var StudentDataChart =  {
            rows : StudentData.rows,
            columns : StudentData.columns,
            options : {title: "AM Survey", hAxis: {title: 'Day', minValue: 0, maxValue: 25}, vAxis: {title: 'Survey Score'}},
            chartType : "LineChart",
            div_id: "StudentData"
        };

        /*var UKGasChart = {
         data : UKGasEmissionsData.dataArray,
         options : {title: "UK Gas Emissions", hAxis: {title: 'Year'}, vAxis: {title: 'Count'}},
         chartType : "LineChart",
         div_id: "UKGasChart"
         };*/
        var mContext = this;
        /*helper.get("/Teams?", mContext)
         .then(function (data) {
         mContext.setState({teams: data});
         });
         helper.get("/users?", mContext)
         .then(function (data) {
         mContext.setState({users: data});
         });
         helper.get("/Surveys?", mContext)
         .then(function (data) {
         mContext.setState({surveys: data});
         });*/
        this.setState({
            'StudentDataChart': StudentDataChart,
        });
    },
    handleChange: function(date) {
        this.setState({
            startDate: date,
        });
    },
    handleTeam: function (e) {
        this.setState({team: e.target.value});
    },
    handleUser: function (e) {
        this.setState({user: e.target.value});
    },
    handleSurvey: function (e) {
        this.setState({survey: e.target.value});
    },
    render: function () {


        return (
            <div className="container">
                <div className = "row">
                    <div className="col-md-4">
                        <label className="control-label" htmlFor="teamId">Team</label>
                        <div className="controls">
                            <TeamList mContext={this}/>
                        </div>
                        <label className="control-label" htmlFor="userId">Team Members</label>
                        <div className="controls">
                            <UserList mContext={this}/>
                        </div>
                        <label className="control-label" htmlFor="surveyId">Survey</label>
                        <div className="controls">
                            <SurveyList mContext={this}/>
                        </div>
                        <label className="control-label" htmlFor="date">Date Range</label>
                        <div className="controls">
                            <DatePicker
                                className="date-black-font"
                                selected={this.state.startDate}
                                maxDate={moment()}
                                 />
                        </div>
                    </div>
                    <div className="col-md-8">
                        <h3> Student Data </h3>
                        <Chart chartType={this.state.StudentDataChart.chartType} width={"500px"} height={"300px"} rows={this.state.StudentDataChart.rows} columns={this.state.StudentDataChart.columns} options = {this.state.StudentDataChart.options} graph_id={this.state.StudentDataChart.div_id}  />
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6">
                        <Link to="/admin/" className="btn btn-primary">Back</Link>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Reports;